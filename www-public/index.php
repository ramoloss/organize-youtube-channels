<?php

namespace Yt;

// Needed because of ROOT_URI
if (file_exists(__DIR__ . '/../inc/_config.php'))
{
    require_once __DIR__ . '/../inc/_config.php';
}
else {
    echo '<h1>Missing config file, base uri assumed to be \'/\'.</h1>';
    define('ROOT_URI', '/');
}

$uri = $_SERVER['REQUEST_URI'];
$p = parse_url($uri, PHP_URL_PATH);
$tpl['g'] = isset($_GET['g'])?preg_replace('([^0-9A-Za-z-_])', '', $_GET['g']):'';
$tpl['id'] = isset($_GET['id'])?preg_replace('([^0-9A-Za-z-_])', '', $_GET['id']):'';
$tpl['tag'] = isset($_GET['tag'])?preg_replace('([^0-9a-z-_])', '', strtolower($_GET['tag'])):'';
$tpl['d'] = $_GET['d']??null;

$tpl['params']['densitable'] = 0;
$tpl['params']['tags'] = 0;

ob_start();
switch($p){
    case ROOT_URI.'editor':
        $tpl['params']['densitable'] = 1;
        $tpl['page'] = 'editor';
        require '../tpl/editor.php';
    break;
    case ROOT_URI.'':
    case ROOT_URI.'gallery':
        $tpl['params']['densitable'] = 1;
        $tpl['params']['tags'] = 1;
        $tpl['page'] = 'gallery';
        require '../tpl/gallery.php';
        break;
    case ROOT_URI.'playlists':
        $tpl['page'] = 'playlists';
        require '../tpl/playlists.php';
        break;
    case ROOT_URI.'sub':
        $tpl['page'] = 'sub';
        require '../tpl/subscriptions.php';
        break;
    case ROOT_URI.'401':
        $tpl['page'] = '401';
        require '../tpl/err/401.php';
        break;
    case ROOT_URI.'403':
        $tpl['page'] = '403';
        require '../tpl/err/401.php';
        break;
    default:
        $tpl['page'] = '404';
        require '../tpl/err/404.php';
        break;
}

$tpl['content'] = ob_get_clean();

// Jusqu'ici rien n'est envoyé, le contenu est stocké dans $tpl['content']
// qui sera affiché au milieu du layout appelé ci-dessous
require '../inc/_layout.php';
