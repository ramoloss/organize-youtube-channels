<?php
namespace Yt;
require '../inc/_inc.php';

use Exception;

$f = new Form();

$tpl['title'] = 'Get my subscriptions';

?>
	<p><em>This functionnality always uses Google API (Invidious doesn't provide some required data).</em></p>
	<form action="" method="get" class="subscriptions">
		<?php
		echo $f->text('id', 'Channel ID:', '', false)
			.$f->submit('List subscriptions');
			
		echo $f->text('url', 'or Channel URL:', '', false)
			.$f->submit('List subscriptions');
		?>
	</form>
<?php 

try
{
	$api =  Api::getInstance();
	$api->setUrl('https://youtube.googleapis.com/youtube/v3/');
	$api->setKey(API_KEY);

	// Get channel id
	if (isset($_GET['url']))
	{
		$tpl['id'] = $db->getChanIdFromUrl($_GET['url']);
		if (!strpos($tpl['id'], 'UC')) // If string start by 'UC' assume it's a channel ID not an username
		{
			foreach ($api->get('channels',
				[ 'part' => 'snippet',
					'fields' => 'items/snippet/title,items/id',
					'forUsername' => $tpl['id'] ])
				->items as $item)
			{
				if ($item->snippet->title == $tpl['id'])
				{
					$tpl['id'] = $item->id;
					break;
				}				
			}
		}
	}

	// Demande d'ajout de liste
	// Demande de mise à jour de la liste
	if (isset($_POST['s_add']) && $tpl['id'] == $_POST['user_id'])
	{
		if ($r = $db->checkSubUser($tpl['id']))
		{
			foreach ($r as $v)
			{
				$tpl['success'][] = 'New sub: '.$v['title'];
			}	
		}
		else
		{
			$tpl['success'][] = 'No updates';
		}
		$db->saveXML();
	}

	
    // **** Display ****

    echo '<h3>Saved user subscriptions:</h3>';

    if ($db->selSubs())
    {
        echo '<ul>';
        foreach ($db->getSelected()->children() as $user)
        {
            echo '<li><a href="?id='.$user['id'].'">';
            echo $user['title'];
            echo '</a></li>';
        }
        echo '</ul>';
    }
    else
    {
        echo 'No playlist tracked at the moment.';
    }   

	// **** Display selected channel ****

	if (!empty($tpl['id']))
	{	
		$id = $tpl['id'];

		// Si on a pas déjà cette liste enregistrée, alors on demande à l'API
		if (false === $db->selSubUser($id))
		{
			[$info, $items] = $db->scrapSubUserById($id);

			echo '<h3>'.$info['title'].' subscribed to:</h3>';

			?>
			<form action="" method="post">
				<input type="hidden" name="user_id" value="<?= $id ?>"/>
				<?= $f->submit('Add this user/list', 's_add');?>
			</form>
			<?php


			echo '<ul class="subscriptions">';
			foreach ($items as $chanId => $val)
			{
				echo 
					'<li>'
						.'<img src="'.$val['thumb'].'"/>'
						.'<div>'
							.$val['title'].' ';
							$link = 'https://www.youtube.com/channel/'.$chanId;
							echo '<a href="'.$link.'">'.$link.' </a>'
						.'</div>'
					.'</li>';
			}
			echo '</ul>';
			echo $info['items'].' items get in '.$info['requests'].' requests';
		}
		else // Si elle est déjà enregistrée
		{
			$info = $db->getSubUserInfo();
			$user = $db->getSelected();

			// Demande d'enregistrement des status
			if (isset($_POST['s_update_user']) && $id == $_POST['user_id'])
			{
				$data = [];
				if (isset($_POST['ch']))
				{
					foreach ($_POST['ch'] as $k => $v)
					{
						$data[$k] = 1;
					}
				}
				$db->writeSubUserStatus($data)->saveXML();
			}

			echo '<h3>'.$info['title'].' subscribed to:</h3>';

			?>
			<form action="" method="post">
				<input type="hidden" name="user_id" value="<?= $id ?>"/>
				<?= $f->submit('Look for new subscriptions', 's_add');?>
			</form>
			<form action="" method="post">
				<input type="hidden" name="user_id" value="<?= $id ?>"/>
				<?= $f->submit('Update statuses', 's_update_user');?>
			<?php


			echo '<ul class="subscriptions">';
			foreach ($user->children() as ['channelId' => $chI, 'title' => $chT, 'thumb' => $chTh, 'status' => $chS])
			{
				$link = 'https://www.youtube.com/channel/'.$chI;

				echo '<input type="checkbox" name="ch['.$chI.']" id="ch['.$chI.']" '.((int)$chS?'checked':'').'>'
					.'<label for="ch['.$chI.']">'
						.'<li class="'.((int) $chS?'added':'').'">'
							.'<img src="'.$chTh.'"/>'
							.'<div>'
								.$chT.' '
								.'<a href="'.$link.'">'.$link.' </a>'
								.'<br>'
								.((int) $chS?'Traité':'Non traité')
							.'</div>'
						.'</li>'
					.'</label>';
			}
			echo '</ul>';

			?>
			<br>
				<input type="hidden" name="user_id" value="<?= $id ?>"/>
				<?= $f->submit('Update statuses', 's_update_user');?>
			</form>
			<?php
		}  
		
	}
}
catch (Exception $e)
{
	$tpl['err'][] = '<pre>'.$e.'</pre>';
}
