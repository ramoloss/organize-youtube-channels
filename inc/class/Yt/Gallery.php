<?php

namespace Yt;

use \DOMDocument;
use DOMXPath;
use \Exception;
use \SimpleXMLElement;

class Gallery {

    private $dom = null;
	private $selected = null;
	private $names = [];
	private $errors = [];
	public $watch_url = 'https://www.youtube.com';

    static protected $_instance = null;

    static public function getInstance()
    {
        return self::$_instance ?: self::$_instance = new Gallery();
    }

    public function __construct(string $file = XML_FILE)
    {
		$this->loadXMLFile($file);
		
		if($this->dom->invidious['enabled'] == 1)
		{
			$this->watch_url = $this->dom->invidious['url'];
		}
    }

    /**
     * Load XML file
     * @param string $p path of file to open
     * @return boolean true if success, false if failed and write to errors[]
     */
    public function loadXMLFile(string $p):bool
    {
        if(!file_exists($p))
        {
            $this->setError("File $p doesn't exist.");
            return false;
        }

        $this->dom = simplexml_load_file($p);

        if($this->dom === false){
            foreach(libxml_get_errors() as $error) {
                $this->setError('l'.$error->line.': '.$error->message);
            }
            libxml_clear_errors();
            $this->dom = null;
            return false;
		}
		$this->selected = $this->dom;
		$this->listNames();
        return true;
    }

	public function loadXMLString(string $s):bool
	{
		if(!($xml = simplexml_load_string($s)))
		{
			foreach(libxml_get_errors() as $error) {
				$this->setError('l'.$error->line.': '.$error->message);
			}
			libxml_clear_errors();
			return false;
		}
		$this->dom = $xml;
		$this->selected = $this->dom->categories;
		$this->listNames();
		return true;
	}

	public function getNameById(string $id):string 
	{
		return $this->getCatById($id)['name'];
	}
	
	public static function getXMLParent(SimpleXMLElement $e)
	{
		return $e->xpath('..');
	}

    /**
	 * @param string $id id of category looked for
	 * @param mixed $e 
	 * @return mixed false if no cat were found, or the simpleXmlElement
     */
	public function getCatById(string $id, $e = null)
    {
		if($id == '')
		{
			$this->selected = $this->dom->categories;
			return $this;
		}

		if ($r = $this->dom->categories->xpath('//cat[@id="'.$id.'"]'))
		{
			return $r[0];
		}
		else
		{
			$this->setError("Category '$id' not found.");
			// return $this->dom->categories;
			return false;
		}
    }
	
	public function selRoot()
	{
		$this->selected = $this->dom->categories;
		return $this;
	}
	
    public function selCatById(string $id)
    {
		if($id == '')
		{
			$this->selected = $this->dom->categories;
			return $this;
		}

		if ($r = $this->dom->categories->xpath('//cat[@id="'.$id.'"]'))
		{
			$this->selected = $r[0];
		}
		else
		{
			$this->selected = $this->dom->categories;
			$this->setError("Category '$id' not found.");
		}
		return $this;
    }
	
	public function selByName(string $n, string $p = '')
	{
		if($n == '')
		{
			$this->selected = $this->dom->categories;
			return $this;
		}

		if ($p === '')
		{
			$e = $this->dom->categories;
		}
		else
		{
			$e = $this->getCatById($p);
		}

		if ($r = $e->xpath('//*[@name="'.$n.'"]'))
		{
			$this->selected = $r[0];
		}
		else
		{
			$this->selected = $this->dom->categories;
			$this->setError("Object named '$n' not found.");
		}
		return $this;
	}

    public function selParent()
    {
		$this->selected = $this->selected->xpath('..')?:$this->dom->categories;
        $this->selected = $this->selected[0];
        return $this;
    }
    
    public function getOrder()
    {
        return $this->selected['order'];
    }
    
    public function getName()
    {
        return $this->selected['name'];
    }
    
    public function getPP()
    {
        return $this->selected['pp'];
    }
    
    public function getLink()
    {
        return $this->selected['link']??false;
    }

    public function getId()
    {
        return $this->selected['id']??false;
    }

    public function getSelected()
    {
        return $this->selected;
    }

    public function list()
    {
        foreach($this->selected as $child)
		{
			$isCat = isset($child['id']) ? true:false;
			$link = $isCat ? "?g=".$child['id']:$child['link'];
			echo $child['order']." - ".($isCat?"CATEGORY - ":'CHANNEL - ').$child['name']." - ".$link." - ".$child['pp']."<br>";
		}
    }

    public function getOrderedList()
    {
		if(count($this->selected->children()) === 0)
		{
			return [];
		}
	
		$i = 0;
        foreach($this->selected->children() as $child)
		{
            $isCat = isset($child['id']) ? true:false;
            $list[] = array('isCat' => ($isCat?true:false),
                            'order' => $child['order']->__toString(),
                            'link' => isset($child['link'])?$child['link']->__toString():'',
                            'id' => isset($child['id'])?$child['id']->__toString():'',
                            'name' => $child['name']->__toString(),
							'pp' => $child['pp']->__toString(),
							'tags' => $child['tags']->__toString());

			$i++;
		}
        
        $listIds = array();
        foreach ($list as $k=>$row)
        {
            $listIds[$k] = $row['order'];
        }
        array_multisort($listIds, SORT_ASC, $list); // Tri dans l'ordre croissant des éléments selon leur ID
        unset($listIds);
		
		return $list;
    }
	
	public function formatedList($thumb = true, $catOnly = false)
	{
		if(count($this->selected->children()) === 0)
		{
			echo 'No elements in this category for now.';
			return $this;
		}
		
		$list = $this->getOrderedList();

		$suf = isset($_GET['d']) ? '?d='. $_GET['d'].'&':'?';

		if($thumb)
		{
			foreach($list as $child)
			{
				$link = $child['isCat'] ? $suf."g=".$child['id']:str_replace('https://www.youtube.com', $this->watch_url, $child['link']);
				
				echo '<div class="thumbnails"><a href="'.$link.'"><img src="'.$child['pp'].'"/><p>'
				.($child['isCat']?'<b>'.$child['name'].'</b>':$child['name']).'</p></a></div>';
			}
		}
		else
		{
			echo '<ul>';
			foreach($list as $child)
			{
				$link = $child['isCat'] ? $suf."g=".$child['id']:str_replace('https://www.youtube.com', $this->watch_url, $child['link']);
				if($catOnly && $child['isCat'])
				{
					echo '<div class="thumbnails"><a href="'.$link.'"><p>'
						.($child['isCat']?'<b>'.$child['name'].'</b>':$child['name']).'</p></a></div>';
				}
				elseif(!$catOnly)
				{
					echo isset($this->selected['name']) ? 
						'<h2>'.$this->selected['name'].'</h2>':
						'<h2>Root</h2>';
						
					echo '<div class="thumbnails"><a href="'.$link.'"><p>'
						.($child['isCat']?'<b>'.$child['name'].'</b>':$child['name']).'</p></a></div>';
				}		
			}
			echo '</ul>';
		}


		return $this;
	}

    public function getAriane($type = [])
    {
		if ($type == [])
		{
			$a = $this->selected->xpath('..');
			$arr['?g='.(string)$this->selected['id']] = (string)$this->selected['name'];
			while($a)
			{
				$a = $a[0];
				$arr[(string)'?g='.$a['id']] = (string)$a['name'];
				$a = $a->xpath('..');         
			}
			$arr['?g='] = 'Root';
			return array_reverse($arr);
		}
		elseif (isset($type['editor']))
		{
			$a = $this->selected->xpath('..');
			$arr['editor?g='.(string)$this->selected['id']] = (string)$this->selected['name'];
			while($a)
			{
				$a = $a[0];
				$arr[(string)'editor?g='.$a['id']] = (string)$a['name'];
				$a = $a->xpath('..');         
			}
			$arr['editor?g='] = 'Root';
			return array_reverse($arr);
		}
		elseif (isset($type['tag']))
		{
			return ['?tag='.$type['tag'] => 'Tag: '. $type['tag']];
		}
		elseif (isset($type['playlist']))
		{
			return ['playlists?id='.$type['playlist'] => 'Playlist: '. $type['playlist']];
		}
		elseif (isset($type['sub']))
		{
			return ['sub?id='.$type['sub'] => 'Sub: '. $type['sub']];
		}
	}

    public function formatAriane($type = [])
    {
		$suf = isset($_GET['d']) ? '&d='. $_GET['d']:'';
		
        $arr = $this->getAriane($type);
		echo '<a href="?'.$suf.'">Root</a> > ';

		if (empty($type) or isset($type['editor']))
		{
			array_shift($arr);
		}
		// var_dump($arr);	
  
		foreach($arr as $k=>$v)
        {
			if (empty($v))
				continue;
			echo '<a href="'.ROOT_URI.$k.$suf.'">'.$v.'</a> > ';
		}
		
		return $this;
    }

	public function getFormatedAriane()
	{
		ob_start();
		$this->formatAriane();
		return ob_get_clean();
	}

	public function setOrder(int $v)
	{
		$this->selected['order'] = $v;
		return $this;
	}

	public function setName(string $n)
	{
		$this->selected['name'] = $n;
		return $this;
	}
	
	public function setLink(string $s)
	{
		isset($this->selected['link'])? $this->selected['link'] = $s:null;
		return $this;
	}
	
	public function setId(string $s)
	{
		isset($this->selected['id'])? $this->selected['id'] = $s:null;
		return $this;
	}

	public function setPP(string $s)
	{
		$this->selected['pp'] = $s;
		return $this;
	}
	
	public function setTags(string $s)
	{
		$this->selected['tags'] = trim(preg_replace('([^0-9a-z-_,])', '', strtolower($s)));
		$this->addTagsToList($this->selected['tags']);
		return $this;
	}

	public function addTagsToList(string $s)
	{
		if (!isset($this->dom->tags))
		{
			$this->dom->addChild('tags');
		}

		$existing = (array) $this->dom->tags->children();
		if (!isset($existing['tag']))
		{
			$existing = false;
		}

		if (!is_array($existing['tag']))
		{
			$existing['tag'] = [$existing['tag']];
		}
		
		$tags = preg_split('/,/', $s);
		foreach ($tags as $tag)
		{
			$tag = trim(strtolower($tag));
			if (empty($tag))
			{
				continue;
			}

			if (!$existing OR !in_array($tag, $existing['tag']))
			{
				$e = $this->dom->tags->addChild('tag', $tag);
			}
		}

		return $this;
	}

	public function getElementsTagged(string $tag, $err = true)
	{
		if (!$r = $this->dom->categories->xpath('//*[contains(@tags, "'.$tag.'")]'))
		{
			if ($err)
			{
				$this->setError('No elements tagged '. $tag . ' (you can remove its node manually in the XML editor)');
			}
			return false;
		}

		return $r;
	}

	public function migrateAddTags()
	{
		
		if (!$r = $this->dom->categories->xpath('//cat[not(@tags)]|//chan[not(@tags)]'))
		{
			return false;
		}

		foreach ($r as $e)
		{
			$e[0]->addAttribute('tags', '');
		}
		$this->saveXML();
		
		return true;
	}

	public function formatElementsTagged(string $tag)
	{
		$list = $this->getElementsTagged($tag);
		
		foreach($list as $child)
		{
			$isCat = isset($child['id']) ? true:false;
			
			$link = $isCat ? "?g=".$child['id']:str_replace('https://www.youtube.com', $this->watch_url, $child['link']);
			
			echo "<div class='thumbnails'><a href='".$link."'><img src='".$child['pp']."'/><p>"
			.($isCat?"<b>".$child['name']."</b>":$child['name'])."</p></a></div>";
		}
	}

	public function listExistingTags()
	{
		if (!isset($this->dom->tags))
		{
			return false;
		}
		
		if (!$children = $this->dom->tags->children())
		{
			return false;
		}
		echo '<details>';
		echo '<summary>Existing tags</summary>';
		foreach ($children as $tag)
		{
			echo '<a href="?tag='.$tag.'">'.$tag.'</a>';
		}
		echo '</details>';
	}

	public function getExistingTags()
	{
		if (!isset($this->dom->tags))
		{
			return false;
		}
		
		if (!$children = $this->dom->tags->children())
		{
			return false;
		}

		foreach ($children as $tag)
		{
			$tags[] = $tag;
		}

		return $tags;
	}
	
	public function removeUnusedTags()
	{
		if (!isset($this->dom->tags))
		{
			return false;
		}
		
		if (!$tags = $this->getExistingTags())
		{
			return false;
		}
		
		$removed = [];
		foreach ($tags as $tag)
		{
			if (!$this->getElementsTagged($tag, false))
			{
				$removed[] = (string) $tag;
				$e = $this->dom->tags->xpath('tag[contains(text(),"'.$tag.'")]');
				unset($e[0][0]);
			}
		}

		return $removed;
	}

	public function addCat(int $order, string $id, string $name, string $pp = '')
	{
		if($this->selected->getName() != 'cat' && $this->selected->getName() != 'categories')
		{
			$this->selParent();
		}

		
		if (preg_match('/bandcamp.com/', $name))
		{
			[$name, $pp] = $this->getBcInfo($name);
		}

		$this->selected = $this->selected->addChild('cat',' ');
		$this->selected->addAttribute('order', $order);
		$this->selected->addAttribute('id', $id);
		$this->selected->addAttribute('name', $name);
		$this->selected->addAttribute('pp', $pp);
		$this->selected->addAttribute('tags', '');
		
		return $this;
	}

	public function addChan(int $order, string $link, string $name = '', string $pp = '')
	{
		if($this->selected->getName() != 'cat' && $this->selected->getName() != 'categories')
		{
			$this->selParent();
		}

		if (empty(trim($link)))
		{
			throw new Exception('Link empty');
		}

		if (preg_match('/youtube.com/', $link))
		{
			if($name === '')
			{
				$name = $this->getChanName($link);
			}
			if($pp === '')
			{
				$pp = $this->getChanThumb($link);
			}
		}
		elseif (preg_match('/bandcamp.com/', $link))
		{
			[$name, $pp] = $this->getBcInfo($link);
		}
		else
		{
			$name = $this->getPagetitle($link);
		}
		
		if(empty($name))
		{
			$name = 'name not found';
		}
		
		$this->selected = $this->selected->addChild('chan');
		$this->selected->addAttribute('order', $order);
		$this->selected->addAttribute('name', $name);
		$this->selected->addAttribute('link', $link);
		$this->selected->addAttribute('pp', $pp);
		$this->selected->addAttribute('tags', '');
		
		return $this;
	}
	
	public function rmSelected()
	{
		unset($this->selected[0][0]);
		$this->selected = $this->dom->categories;
		return $this;
	}

    public function updatePP()
    {
		$this->selected['pp'] = $this->getChanThumb($this->selected['link']);
		return $this;
	}

	public function getChanName($url)
	{
		$api = Api::getInstance();
		// For YT API:
		$api->setUrl('https://youtube.googleapis.com/youtube/v3/');
		$api->setKey(API_KEY);

		if ($id = $this->getChanIdFromUrl($url) )
		{
			// YT API

			// Channel Id
			if (substr($id, 0, 2) == 'UC')
			{
				return $api->get('channels',
					[ 'part' => 'snippet',
					'fields' => 'items/snippet/title',
					'id' => $id ])
					->items[0]->snippet->title;
			}
			else // Username
			{
				return $api->get('channels',
					[ 'part' => 'snippet',
					'fields' => 'items/snippet/title',
					'forUsername' => $id ])
					->items[0]->snippet->title;
			}

			// Invidious API
			// return $api->get('channels/'.$id, ['fields' => 'author'] )->author;
		}
		else
		{
			$this->setError('Could not get name of '.$url);
			return false;
		}
	}


	public function getChanThumb($url)
	{
		$api = Api::getInstance();
		// For YT API:
		$api->setUrl('https://youtube.googleapis.com/youtube/v3/');
		$api->setKey(API_KEY);

		if ($id = $this->getChanIdFromUrl($url) )
		{
			// YT API
			
			// Channel Id
			if (substr($id, 0, 2) == 'UC')
			{
				$r = (array) $api->get('channels',
					[ 'part' => 'snippet',
						'fields' => 'items/snippet/thumbnails',
						'id' => $id ])
						->items[0]->snippet->thumbnails;
			}
			else
			{
				$r = (array) $api->get('channels',
					[ 'part' => 'snippet',
						'fields' => 'items/snippet/thumbnails',
						'forUsername' => $id ])
						->items[0]->snippet->thumbnails;
			}

			return $r[array_key_last($r)]->url;
			
			// Invidious API
			// $r = $api->get('channels/'.$id, ['fields' => 'authorThumbnails'] )->authorThumbnails;
			// return $r[array_key_last($r)]->url;
		}
		else
		{
			$this->setError('Could not get thumbnail of '.$url);
			return false;
		}
	}

	public function getBcInfo($url)
	{
		if (!$html = file_get_contents($url))
		{
			$this->setError('Could not fetch info of '. $url);
			return false;
		}
		
		$doc = new DOMDocument();
		if (!$doc->loadHTML($html))
		{
			$this->setError('Could not parse page '. $url);
			return false;
		}

		$name = $doc->getElementsByTagName('title')->item(0)->nodeValue;

		// Try to get album image
		$pp = $doc->getElementById('tralbumArt');
		if (null !== $pp)
		{
			$pp = $pp->childNodes->item(1)->childNodes->item(1)->attributes->getNamedItem('src')->value;
		}
		else // Or get band image
		{
			$finder = new DOMXPath($doc);
			$pp = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'band-photo ')]");
			$pp = $pp->item(0)->attributes->getNamedItem('src')->value;
		}

		$name = str_replace('Music | ', '', $name);

		return [$name, $pp];
	}

	public function getPagetitle($url)
	{		
		if (!$html = file_get_contents($url))
		{
			$this->setError('Could not fetch info of '. $url);
			return false;
		}
		
		$doc = new DOMDocument();
		if (!$doc->loadHTML($html))
		{
			$this->setError('Could not parse page '. $url);
			return false;
		}

		return $doc->getElementsByTagName('title')->item(0)->nodeValue;
	}

	public function getChanIdFromUrl($url = '')
	{
		if (empty($url))
		{
			$this->setError('Empty url passed.');
			return false;
		}

		// Custom URLs '/c/TrucMuche'
		if (preg_match('/(?<=c\/)(.+?)(?=\/)/', $url . '/', $id))
		{
			$api = Api::getInstance();
			// For YT API:
			$api->setUrl('https://youtube.googleapis.com/youtube/v3/');
			$api->setKey(API_KEY);
			
			$r = $api->get('search',
				[ 'part' => 'id,snippet',
				'type' => 'channel',
				'fields' => 'items/snippet/channelId',
				'q' => $id[0] ])
				->items[0]->snippet->channelId;
				
			return trim($r);
		}

		if (preg_match('/(?<=channel\/|user\/)(.+?)(?=\/)/', $url . '/', $id))
		{
			return trim($id[0]);
		}
		else
		{
			$this->setError('Could not get id of '.$url);
			return false;
		}
	}

	public function getPlaylistIdFromUrl($url = '')
	{
		if (empty($url))
		{
			$this->setError('Empty url passed.');
			return false;
		}

		if (preg_match('/(?<=list=)(.+)(?=&)/', $url.'&', $id))
		{
			return $id[0];
		}
		else
		{
			$this->setError('Could not get list id of '.$url);
			return false;
		}
	}

	public function isNameFree(string $s)
	{
		foreach($this->names as $name)
		{
			if($s == $name)
			{
				return false;
			}
		}
		return true;
	}

	public function listNames()
	{
		$rec = function ($e) use (&$rec)
		{
			if($e->count() > 0)
			{
				foreach($e->children() as $f)
				{
					if(isset($f['name']))
					{
						$this->names[] = (string) $f['name'];
					}
					if(isset($f['id']))
					{
						$rec($f);
					}
				}
			}
		};

		$rec($this->selected);

		return $this;
	}

	public function getMaxOrder()
	{
		if(count($this->selected->children()) === 0)
		{
			return 0;
		}
	
		$max = 0;
        foreach($this->selected->children() as $child)
		{
			if ($max < (int) $child['order'])
			{
				$max = (int) $child['order'];
			}
		}
        
		return $max;
	}

	public function saveXML(string $f = XML_FILE)
	{
		//Format XML to save indented tree rather than one line and save
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($this->dom->asXML());
		$dom->save($f);
		return $this;
	}

	public function asXML()
	{	
		return $this->dom->asXML();
	}

    public function getErrors():array
    {
        return $this->errors;
    }

    private function setError($s)
    {
        // $this->errors[(string)debug_backtrace()[1]['function']] = $s;
        $this->errors[] = $s;
    }

	public function getInvidiousSettings()
	{
		return $this->dom->invidious;
	}

	public function setInvidiousSettings($enable, $url = '')
	{
		$this->dom->invidious['enabled'] = $enable ? '1':'0';
		$this->dom->invidious['url'] = $url;
	}

	// **** Playlists ****

	public function scrapPlaylistById($id = '')
	{
		if (!empty($id))
		{
			// Dropping Invidious API because of max playlist items returned (and less stable)
			$api = Api::getInstance();
			
			$pl = $api->get('playlists', ['id' => $id, 'part' => 'snippet', 'fields' => 'items(snippet(channelTitle,title))']);
			$info['playlistId'] = $id;
			$info['title'] = $pl->items[0]->snippet->title;
			$info['author'] = $pl->items[0]->snippet->channelTitle;

			$params = [
				'part' => 'id,snippet',
				'fields' => 'nextPageToken,pageInfo,items(snippet(resourceId/videoId))',
				'playlistId' => $id,
			];
				

			$info['items'] = $info['requests'] = 0;

			$continue = true;
			while ($continue)
			{
				$r = $api->get('playlistItems', $params);
				$info['requests']++;

				if (isset($r->nextPageToken))
				{
					$params['pageToken'] = $r->nextPageToken;   
				}
				else
				{
					$continue = false;
				}

				foreach ($r->items as $item)
				{
					$videoId = (string) $item->snippet->resourceId->videoId;
					$vid = $api->get('videos', ['id' => $videoId, 'part' => 'id,snippet', 'fields' => 'items(id,snippet(channelTitle,title))']);

					if (isset($vid->items[0]))
					{
						$data[$videoId]['title'] = $vid->items[0]->snippet->title;
						$data[$videoId]['author'] = $vid->items[0]->snippet->channelTitle;
						$info['items']++;
					}
				} 
			}
				
			return [$info, $data];
		}
		else
		{
			throw new Exception('Could not get playlist '.$id);
		}
	}

	public function selPlaylist($id = '')
	{
		if (!$r = $this->getPlaylist($id))
		{
			return false; // Playlist doesn't exist
		}
		
		$this->selected = $r;
		return $this;
	}

	public function getPlaylist($id = '')
	{
		if (!isset($this->dom->playlists))
		{
			return false;
		}

		$e = $this->dom->playlists;
		
		foreach ($e->children() as $playlist)
		{
			if ($id == $playlist['playlistId'])
			{
				return $playlist;
			}
		}

		return false; // Playlist doesn't exist
	}

	public function getPlaylistInfo($plId = '')
	{
		if (!empty($plId))
		{
			$r = $this->getPlaylist($plId);
		}
		else
		{
			$r = $this->selected;
		}

		if (!$r)
		{
			return false; // Playlist doesn't exist
		}
		
		return [
				'title' => $r['title'],
				'author' => $r['author'],
			];
	}

	public function addPlaylist($info)
	{
		if (!isset($this->dom->playlists))
		{
			$this->dom->addChild('playlists');
		}

		$this->selected = $this->dom->playlists->addChild('playlist');
		$this->selected->addAttribute('playlistId', $info['playlistId']);
		$this->selected->addAttribute('title', $info['title']);
		$this->selected->addAttribute('author', $info['author']);

		return $this;

	}

	public function addChanges($data)
	{
		$e = $this->selected->addChild('changes');
		$e->addAttribute('time', time());

		foreach ($data as $id => $val)
		{
			$v = $e->addChild('video');
			$v->addAttribute('videoId', $id);
			if (isset($val['title']) && isset($val['author']) )
			{
				$v->addAttribute('title', $val['title']);
				$v->addAttribute('author', $val['author']);
			}
			if (!isset($val['status']))
			{
				$val['status'] = 'added';
			}
			$v->addAttribute('status', $val['status']);
		}

		return $this;
	}

	public function buildPlaylist($plId = '')
	{
		if (!empty($plId))
		{
			$children = $this->getPlaylist($plId)->children();
		}
		else
		{
			$children = $this->selected->children();
		}
		
		$hist = [];
		foreach ($children as $changes)
		{
			foreach ($changes as $video)
			{
				if ($video['status'] == 'added' || $video['status'] == 'edited')
				{
					$hist[(string) $video['videoId']] = ['title' => (string) $video['title'], 'author' => (string) $video['author']];
				}
				elseif ($video['status'] == 'removed' && array_key_exists((string) $video['videoId'], $hist))
				{
					unset($hist[(string) $video['videoId']]);
				}
			}
		}

		return $hist;
	}

	public function selPlaylists()
	{
		if (!isset($this->dom->playlists))
		{
			return false;
		}

		$this->selected = $this->dom->playlists;

		return $this;
	}

	public function getPlaylists()
	{
		if (!isset($this->dom->playlists))
		{
			return false;
		}

		
		return $this->dom->playlists;
	}

	public function checkPlaylist($plId = '')
	{
		// Peut-être il faudrait vérif/gérer des erreurs un peu plus ?
		
		[$info, $current] = $this->scrapPlaylistById($plId);
		if (empty($info) || empty($current))
		{
			throw new Exception('Invalid data scrapped.');
		}

		$plId = $info['playlistId'];

		// If playlist doesn't exists yet
		if (false === $this->selPlaylist($plId))
		{
			$this->addPlaylist($info)->addChanges($current);
			return true; // ??
		}
		
		// Rebuild current playlist status from changes tags
		$hist = $this->buildPlaylist();

		// Parses differences between saved and scrapped playlists
		foreach ($hist as $id => $video)
		{
			if (!array_key_exists($id, $current)) // Existait dans l'historique mais plus dans la nouvelle
			{
				$changes[$id] = [
					'title' => $video['title'],
					'author' => $video['author'],
					'status' => 'removed',
				];
			}
			else // Still exists
			{
				if ($video['title'] != $current[$id]['title'] || $video['author'] != $current[$id]['author'])
				{
					$changes[$id] = [
						'title' => $current[$id]['title'],
						'author' => $current[$id]['author'],
						'status' => 'edited',
					];
				}

				$processed[] = $id;
			}
		}
		foreach ($current as $id => $video)
		{
			if (in_array($id, $processed))
			{
				continue; // déjà traité
			}

			if (array_key_exists($id, $hist)) // Check s'il y en aurait pas passé au travers du filet ?
			{
				$this->setError('This video should already be processed :(');
			}

			$changes[$id] = [
					'title' => $video['title'],
					'author' => $video['author'],
				];
		}

		// Apply changes
		if(isset($changes) && !empty($changes))
		{
			$this->addChanges($changes);
			return true;
		}
		
		return false;
	}

	// **** Subscriptions ****

	public function selSubUser($id = '')
	{
		if (!$r = $this->getSubUser($id))
		{
			return false; // Playlist doesn't exist
		}
		
		$this->selected = $r;
		return $this;
	}

	public function getSubUser($id = '')
	{
		if (!isset($this->dom->subscriptions))
		{
			return false;
		}

		if (empty($id))
		{
			return false;
		}

		$e = $this->dom->subscriptions;
		
		foreach ($e->children() as $user)
		{
			if ($id == $user['id'])
			{
				return $user;
			}
		}

		return false; // doesn't exist
	}

	public function getSubUserInfo($id = '')
	{
		if (!empty($id))
		{
			$r = $this->getSubUser($id);
		}
		else
		{
			$r = $this->selected;
		}

		if (!$r)
		{
			return false; // doesn't exist
		}
		
		return [
				'title' => (string) $r['title'],
				'id' => (string) $r['id'],
			];
	}

	public function addSubUser($info)
	{
		if (!isset($this->dom->subscriptions))
		{
			$this->dom->addChild('subscriptions');
		}

		$this->selected = $this->dom->subscriptions->addChild('user');
		$this->selected->addAttribute('id', $info['id']);
		$this->selected->addAttribute('title', $info['title']);

		return $this;
	}

	public function writeSubUserChans($data)
	{
		// Remove children
		// Remove node then remove recreate it
		$info = $this->getSubUserInfo();
		unset($this->selected[0]);
		$this->addSubUser($info);

		foreach ($data as $id => $val)
		{
			$v = $this->selected->addChild('channel');
			$v->addAttribute('channelId', $id);
			$v->addAttribute('title', $val['title']);
			$v->addAttribute('thumb', $val['thumb']);
			$v->addAttribute('status', $val['status']??0);
		}

		return $this;
	}

	public function writeSubUserStatus($data)
	{
		foreach ($this->selected->children() as $chan)
		{
			$chan['status'] = $data[(string) $chan['channelId'] ] ?? 0;
		}

		return $this;
	}

	public function selSubs()
	{
		if (!isset($this->dom->subscriptions))
		{
			return false;
		}

		$this->selected = $this->dom->subscriptions;

		return $this;
	}

	public function getSubs()
	{
		if (!isset($this->dom->subscriptions))
		{
			return false;
		}
		
		return $this->dom->subscriptions;
	}
	
	public function scrapSubUserById($id = '')
	{
		if (!empty($id))
		{
			$api = Api::getInstance();

			$info['id'] = $id;
			$info['title'] = $api->get('channels',
				[ 'part' => 'snippet',
					'fields' => 'items/snippet/title',
					'id' => $id ])
				->items[0]->snippet->title;

			$params = [
				'part' => 'id,snippet',
				'fields' => 'nextPageToken,pageInfo,items(snippet(title,description,resourceId/channelId,thumbnails/default))',
				'channelId' => $id,
			];
			
			$info['items'] = $info['requests'] = 0;


			$continue = true;
			while ($continue)
			// for ($i = 0; $i < 3; $i++)
			{
				$r = $api->get('subscriptions', $params);
				$info['requests']++;

				if (isset($r->nextPageToken))
				{
					$params['pageToken'] = $r->nextPageToken;   
				}
				else
				{
					$continue = false;
				}

				foreach ($r->items as $item)
				{
					$chanId = (string) $item->snippet->resourceId->channelId; 
					$data[$chanId]['title'] = $item->snippet->title;
					$data[$chanId]['thumb'] = $item->snippet->thumbnails->default->url;

					$info['items']++;
				} 
			}
			
			return [$info, $data];
		}
		else
		{
			throw new Exception('Could not get playlist '.$id);
		}
	}

	public function checkSubUser($id = '')
	{
		[$info, $current] = $this->scrapSubUserById($id);

		if (empty($info) || empty($current))
		{
			throw new Exception('Invalid data scrapped.');
		}

		// If user doesn't exists yet
		if (false === $this->selSubUser($id))
		{
			$this->addSubUser($info)->writeSubUserChans($current);
			return false; // ??
		}
		
		// Parses differences between saved and scrapped playlists
		$children = $this->selected->children();
		foreach ($children as ['channelId' => $chanId, 'status' => $status])
		{
			$hist[(string) $chanId] = (int) $status;
		}

		$changes = false;
		foreach ($current as $chanId => $chan)
		{
			if (array_key_exists($chanId, $hist)) // Si elle existe déjà, on récupère le statut pour pas l'écraser
			{
				$current[$chanId]['status'] = $hist[$chanId];
			}
			else // Si c'est une nouvelle, la cautionner pour le toast
			{
				$changes[] = ['channelId' => $chanId, 'title' => $chan['title']];
			}
		}

		// Apply changes
		$this->writeSubUserChans($current);

		return $changes;
	}
}