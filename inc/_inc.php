<?php
define('YT_VERSION', '0.2');

if (file_exists(__DIR__.'/_config.php'))
	require_once __DIR__.'/_config.php';

defined('ROOT_URI')						OR define('ROOT_URI', '/');
defined('XML_FILE')						OR define('XML_FILE', __DIR__.'/../db.xml');
defined('API_KEY')						OR define('API_KEY', '');

// Default values for templates
isset($tpl['title_suffix'])				OR $tpl['title_suffix'] = "My fav Youtube channels";
isset($tpl['description'])				OR $tpl['description'] = "Browse my favs Youtube channels!";

// I can't remember, maybe you need to let that on? #woopsie
libxml_use_internal_errors(true);

if (!file_exists(XML_FILE))
{
	// create it
	$ver = YT_VERSION;
	$base_xml = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<root>
  <meta version="{$ver}"/>
  <invidious enabled="0" url="https://invidious.fdn.fr"/>
  <categories order="0"></categories>
</root>
EOF;

	file_put_contents(XML_FILE, $base_xml);
}

require_once __DIR__.'/_func.php';

require_once __DIR__.'/vendor/autoload.php';

$db = Yt\Gallery::getInstance();

$e = $db->getSelected();
if (!isset($e->meta['version']))
{
	$e->addChild('meta')->addAttribute('version', YT_VERSION);
	$db->migrateAddTags();
}
else
{
	if (version_compare($e->meta['version'], YT_VERSION, '<' ))
	{
		$db->migrateAddTags();
	}	
}

unset($e);