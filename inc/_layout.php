<?php
$tpl['title'] = ($tpl['title'] ? $tpl['title'] . " - " : "") . $tpl['title_suffix'];
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<title><?= $tpl['title'] ?></title>
	<meta name="description" content="<?= $tpl['description'] ?>">
	<meta property="og:title" content="<?= $tpl['title'] ?>">
	<meta property="og:image" content="img/logo-01.png">
	<meta property="og:description" content="<?= $tpl['description'] ?>">
	<link rel="stylesheet" href="<?= ROOT_URI ?>css/style.css">

	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" async></script> -->
	<script src="./js/script.js" defer></script>
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->

</head>

<body <?php if ($tpl['params']['densitable'] && isset($tpl['d']) && !empty($tpl['d']))
	{
		switch ($tpl['d'])
		{
			case 1:
				echo 'class="dense"';
				break;
			case 2:
				echo 'class="dense ultradense"';
				break;
		}
	}?>>
	<nav id="NAV">
		<span>

			<?php	switch ($tpl['page'])
					{
						case 'sub':
							$db->formatAriane(['sub' => $tpl['id']]);
							break;
						case 'playlists':
							$db->formatAriane(['playlist' => $tpl['id']]);
							break;
						case 'editor':
							$db->formatAriane(['editor' => '']);
							break;
						case 'gallery':
						default:
							if (!empty($tpl['tag']))
								$db->formatAriane(['tag' => $tpl['tag']]);
							else
								$db->selCatById($tpl['g'])->formatAriane();
							break;
					}
					
				 ?>

		</span>
		<label class="icon" for="nav-collapse">
			<svg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'>
				<path stroke='rgba(0, 0, 0, 0.7)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22' />
			</svg>
		</label>
		<input id="nav-collapse" class="toggle" type="checkbox" />
		<div class="collapsible">
			<div class="navLinks">
				<?php
				$suf = isset($_GET['d']) ? '&d='. $_GET['d']:'';

				if($tpl['page'] == 'editor'){?>
				<a href="./<?= '?g='.$tpl['g'].$suf; ?>">Galerie</a>
				<?php } else {?>
				<a href="./?<?= $suf ?>">Accueil</a>
				<?php }?>
				<a href="editor<?= '?g='.$tpl['g'].$suf; ?>">Editeur</a>
				<a href="playlists">Playlists</a>
				<a href="sub">Subscriptions</a>
			</div>
		</div>
	</nav>

	<?php // Displays errors
	if (array_key_exists('err', $tpl) && !empty($tpl['err'])) : ?>
		<section class="status">
			<div class="container" style="padding: 0;">
				<div class="alert alert-danger">
					<?php if (is_array($tpl['err'])) {
						echo implode('<br/>', $tpl['err']);
					} else {
						echo $tpl['err'];
					}
					?>
				</div>
			</div>
		</section>
	<?php endif;  ?>
	<?php // Displays successes
	if (array_key_exists('success', $tpl) && !empty($tpl['success'])) : ?>
		<section class="status">
			<div class="container" style="padding: 0;">
				<div class="alert alert-valid">
					<?php if (is_array($tpl['success'])) {
						echo implode('<br/>', $tpl['success']);
					} else {
						echo $tpl['success'];
					}
					?>
				</div>
			</div>
		</section>
	<?php endif;  ?>

	<section>
		<header>
			<?php 
				if ($tpl['params']['densitable'])
				{
					$link = '?';
					foreach ($_GET as $k=>$v)
					{
						if ($k == 'd')
							continue;
						$link .= $k.'='.$v.'&';
					}

					echo '<div class="radio btn"><span>Density</span>';
					foreach ([0,1,2] as $v)
					{
						$class = $tpl['d'] == $v ? ' class="selected"':'';
						echo '<a href="'.$link .'d='. $v.'"'.$class.'>'.$v.'</a>';
					}
					echo	'</div>';
				}
				if ($tpl['params']['tags'])
				{
					$db->listExistingTags();
				}
			?>
		</header>

		<?= $tpl['content'] ?>
	</section>

	<section id="footer">
		<a href="https://gitlab.com/ramoloss/organize-youtube-channels">Published under AGPLv3 on gitlab.com/ramoloss/organize-youtube-channels</a>
	</section>
</body>

</html>