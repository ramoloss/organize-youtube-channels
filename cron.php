<?php
namespace Yt;
require __DIR__.'/inc/_inc.php';

use Exception;


try
{
    $api = Api::getInstance();
    $api->setUrl('https://youtube.googleapis.com/youtube/v3/');
    $api->setKey(API_KEY);
    
    echo "Getting playlists...\n";
    if ($db->selPlaylists())
    {
        foreach ($db->getSelected()->children() as $pl)
        {
            echo $pl['playlistId'] ."\t". $pl['title'] ."\t". $pl['author'];
            echo "\n";
            try
            {

                $r = $db->checkPlaylist($pl['playlistId']);
                if ($r) {
                    $db->saveXML();
                    echo 'Updated';
                }
                else {
                    echo 'No updates';
                }
                echo "\n";
            }
            catch (Exception $e)
            {
                echo "Playlist not updated, exception caught:\n";
                echo $e->getMessage();
                echo "\n";
            }
        }
    }
}
catch (Exception $e)
{
    echo "Global exception caught:\n";
    echo $e->getMessage();
    echo "\n";
}