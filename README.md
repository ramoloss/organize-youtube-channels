# Organize Youtube channels

* Allows to classify Youtube channels into categories and sub-categories.
* Can track playlists changes (avoid being frustrated with "Deleted video" you can't even recall what it was).
* Can import list of subscriptions of a public channel/account, and mark them as processed or not to track if you've already added it to your classification.


* Can redirect to an invidious instance.

For playlists tracking, use Invidious API, but uses Google API for subscriptions lists. You need to specify your API key in inc/_config.php.

When adding a channel, can fetch name and thumbnail automatically.
You can add links other than Youtube channels, it won't break anything (eg.: in a music category, I have some soundcloud links too) but won't fetch infos.

You can easily change style/colors with the 3 CSS variables in :root.

Main feature (classifying channels) was poorly written, I hope I could rewrite more properly soon.


See example here : https://yt.noizette.net/